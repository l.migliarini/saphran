<?php

require_once '../src/model/Model.php';
require_once '../src/model/CountryLanguage.php';
require_once '../src/model/DAO.php';
require_once '../src/model/DAOUser.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAOCountryLanguage
 *
 * @author student
 */
class DAOCountryLanguage extends DAO {

    public function __construct($cnx) {
        parent::__construct($cnx);
    }

    public function find($id): CountryLanguage {
        $SQL = "SELECT * FROM countrylanguage WHERE CountryLanguage_Id =:id";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->bindValue("id", $id);
        $prepareStatement->execute();
        $CountryLanguage = $prepareStatement->fetchObject("CountryLanguage");
        return $CountryLanguage;
    }

    public function findByCoutry($CodeCountry) {
        // TODO: Erreur technique MYSQL : Faire marcher la requete normale ( 1ere requete)   
        //$SQL = "SELECT * FROM countrylanguage WHERE CountryCode = :code";
        $SQL = "SELECT * FROM countrylanguage WHERE CountryCode like :code";

        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->bindValue("code", $CodeCountry);
        $prepareStatement->execute();
        $countryByLanguage = $prepareStatement->fetchAll(PDO::FETCH_ASSOC);
        return $countryByLanguage;
    }

    public function update($countryLanguage) {
        $SQL = "UPDATE countrylanguage SET CountryCode= :CoutryCode, Language= :Language, IsOfficial = :IsOfficial , Percentage= :Percentage where CountryLanguage_Id= :id";


        $prepareStatement = $this->cnx->prepare($SQL);

        $prepareStatement->bindValue("id", $countryLanguage->getCountryLanguage_Id());

        $prepareStatement->bindValue("CoutryCode", $countryLanguage->getCountryCode());
        $prepareStatement->bindValue("Language", $countryLanguage->getLanguage());
        $prepareStatement->bindValue("IsOfficial", $countryLanguage->getIsOfficial());
        $prepareStatement->bindValue("Percentage", $countryLanguage->getPercentage());

        $prepareStatement->execute();
    }

    public function save($countryLanguage) {
        $SQL = "INSERT INTO countrylanguage values (:CoutryLanguage_id, :CoutryCode, :Language, :IsOfficial , :Percentage)";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->bindValue("CoutryLanguage_id", $countryLanguage->getCountryLanguage_Id());
        $prepareStatement->bindValue("CoutryCode", $countryLanguage->getCountryCode());
        $prepareStatement->bindValue("Language", $countryLanguage->getLanguage());
        $prepareStatement->bindValue("IsOfficial", $countryLanguage->getIsOfficial());
        $prepareStatement->bindValue("Percentage", $countryLanguage->getPercentage());

        $prepareStatement->execute();
    }

    public function remove($language) {
        $SQL = "DELETE from countrylanguage where CountryLanguage_Id = :languageId";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->bindValue("languageId", $language->getCountryLanguage_Id());
        $prepareStatement->execute();
    }

    public function findAll(): array {
        $SQL = "SELECT * FROM countrylanguage";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->execute();
        $liste = [];
        while (($data = $prepareStatement->fetchObject("countrylanguage")) != false) {
            array_push($liste, $data);
        }
        return $liste;
    }

    public function count(): int {
        $SQL = "SELECT COUNT(CountryLanguage_Id) FROM coutrylanguage";
        $prepareStatement = $this->cnx->query($SQL);
        $prepareStatement->execute();
        $countrylanguage_count = $prepareStatement->fetchColumn();
        return $countrylanguage_count;
    }
    
    public function findByCode($CodeCountry) : Country{
        $SQL = "SELECT * FROM country WHERE Code =:code";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("code", $CodeCountry);
        $prepareStatement -> execute();
        $country = $prepareStatement -> fetchObject("Country");
        return $country;
    }

}
