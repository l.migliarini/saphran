<?php

require_once '../src/model/Model.php';
require_once '../src/model/Country.php';
require_once '../src/model/DAO.php';

class DAOCountry{
    /** @var $cnx PDO*/
    protected $cnx;
    public function __construct($cnx) {
        $this->cnx = $cnx;
    }
    
    public function find($id) : Country{
        $SQL = "SELECT * FROM country WHERE Country_Id =:id";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("id", $id);
        $prepareStatement -> execute();
        $country = $prepareStatement -> fetchObject("Country");
        return $country;
        
    }
    
    public function save(Country $country) {
        $SQL ="INSERT INTO country (Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2, Image1, Image2) VALUES (:Code, :Name, :Continent, :Region, :SurfaceArea, :IndepYear, :Population, :LifeExpectancy, :GNP, :GNPOld, :LocalName, :GovernmentForm, :HeadOfState, :Capital, :Code2, :Image1, :Image2)";
        
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->bindValue("Code", $country->getCode());
        $prepareStatement->bindValue("Name", $country->getName());
        $prepareStatement->bindValue("Continent", $country->getContinent());
        $prepareStatement->bindValue("Region", $country->getRegion());
        $prepareStatement->bindValue("SurfaceArea", $country->getSurfaceArea());
        $prepareStatement->bindValue("IndepYear", $country->getIndepYear());
        $prepareStatement->bindValue("Population", $country->getPopulation());
        $prepareStatement->bindValue("LifeExpectancy", $country->getLifeExpectancy());
        $prepareStatement->bindValue("GNP", $country->getGNP());
        $prepareStatement->bindValue("GNPOld", $country->getGNPOld());
        $prepareStatement->bindValue("LocalName", $country->getLocalName());
        $prepareStatement->bindValue("GovernmentForm", $country->getGovernmentForm());
        $prepareStatement->bindValue("HeadOfState", $country->getHeadOfState());
        $prepareStatement->bindValue("Capital", $country->getCapital());
        $prepareStatement->bindValue("Code2", $country->getCode2());
        $prepareStatement->bindValue("Image1", $country->getImage1());
        $prepareStatement->bindValue("Image2", $country->getImage2());
        
        $code = $prepareStatement->execute();
        echo "<br>";
        $prepareStatement->debugDumpParams();
        echo "<br>** code : $code";
        echo "<br> error code : " . $prepareStatement->errorCode();
    }
    
    
//    public function update(Country $country) // * Update
//    {
//        $SQL = "UPDATE country SET Code = :code, Name = :name, Continent = :continent, Region = :region, SurfaceArea = :surfaceArea, IndepYear = :indepYear, Population = :populations, LifeExpectancy = :lifeExpectancy, GNP = :gnp, GNPOld = :gnpOld, LocalName= :localName, GovernmentForm = :government, HeadOfState = :headOfState, Capital = :capital, Code2 = :code2 WHERE Country_Id = :cid";
//
//        $prepareStatement = $this->cnx->prepare($SQL);
//        $prepareStatement->bindValue("code", $country->getCode());
//        $prepareStatement->bindValue("name", $country->getName());
//        $prepareStatement->bindValue("continent", $country->getContinent());
//        $prepareStatement->bindValue("region", $country->getRegion());
//        $prepareStatement->bindValue("surfaceArea", $country->getSurfaceArea());
//        $prepareStatement->bindValue("indepYear", $country->getIndepYear());
//        $prepareStatement->bindValue("populations", $country->getPopulation());
//        $prepareStatement->bindValue("lifeExpectancy", $country->getLifeExpectancy());
//        $prepareStatement->bindValue("gnp", $country->getGNP());
//        $prepareStatement->bindValue("gnpOld", $country->getGNPOld());
//        $prepareStatement->bindValue("localName", $country->getLocalName());
//        $prepareStatement->bindValue("government", $country->getGovernmentForm());
//        $prepareStatement->bindValue("headOfState", $country->getHeadOfState());
//        $prepareStatement->bindValue("capital", $country->getCapital());
//        $prepareStatement->bindValue("code2", $country->getCode2());
//        $prepareStatement->bindValue("cid", $country->getCountry_id());
//
//        $prepareStatement->execute();
//    }
    
    public function update($country) {
        $sql = "UPDATE country SET ";
        foreach ($country->getObjectVar() as $key => $value) {
            if ($value != NULL) {
                $sql .= " $key = :$key,";
            }
        }
        $sql = rtrim($sql, ",");
        $sql .= " WHERE Country_Id = :Country_Id";
        $prepareStatement = $this->cnx->prepare($sql);
        foreach ($country->getObjectVar() as $key => $value) {
            if ($value != NULL) {
                $method = "get$key";
                if (method_exists($country, $method)) {
                    $prepareStatement->bindValue($key, $country->$method());
                }
            }
        }
        $prepareStatement->execute();

    }
    
    public function remove($id): void {

        $sqlCity = "DELETE FROM city WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $prepareStatementCity = $this->cnx->prepare($sqlCity);
        $prepareStatementCity->bindValue("Country_Id", $id);
        $codeCity = $prepareStatementCity->execute();

        $sqlLanguage = "DELETE FROM countrylanguage WHERE CountryCode = (SELECT Code FROM country WHERE Country_Id = :Country_Id)";
        $prepareStatementLanguage = $this->cnx->prepare($sqlLanguage);
        $prepareStatementLanguage->bindValue("Country_Id", $id);
        $codeLanguage = $prepareStatementLanguage->execute();

        $sqlCountry = "DELETE FROM country WHERE Country_Id = :Country_Id";
        $prepareStatementCountry = $this->cnx->prepare($sqlCountry);
        $prepareStatementCountry->bindValue("Country_Id", $id);
        $codeCountry = $prepareStatementCountry->execute();
    }
    
    public function findAll(): array
    {
        $SQL = "SELECT * FROM country";
        $prepareStatement = $this->cnx->query($SQL);
        $prepareStatement->setFetchMode(PDO::FETCH_CLASS, 'Country');
        $prepareStatement->execute();
        //$list_ville = $prepareStatement->fetchObject("City");
        $country_list = [];
       while(($data = $prepareStatement ->fetchObject("Country")) != false){
            array_push($country_list,$data);
        }
        return $country_list;
    }
   
    
      public function count(): int
    {
        $SQL = "SELECT COUNT(Country_Id) FROM country";
        $prepareStatement = $this->cnx->query($SQL);
        $prepareStatement->execute();
        $country_count = $prepareStatement->fetchColumn();
        return $country_count;
    }
    
      public function getEnumFromContinent() {
        $SQL = "SELECT DISTINCT Continent FROM country";
        $prepareStatement = $this->cnx->prepare($SQL);
        $prepareStatement->execute();
        $enum = $prepareStatement->fetchAll(PDO::FETCH_ASSOC);
        return $enum;
        
    }
    
    public function getAllCityFromContinent($id) {
        $SQL = "SELECT * FROM country WHERE Continent =:id";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("id", $id);
        $prepareStatement -> execute();
        $country = $prepareStatement -> fetchAll(PDO::FETCH_ASSOC);
        return $country;  
    }
    
    public function findByCode($CodeCountry) : Country{
        $SQL = "SELECT * FROM country WHERE Code =:code";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("code", $CodeCountry);
        $prepareStatement -> execute();
        $country = $prepareStatement -> fetchObject("Country");
        return $country;
    }

        
}