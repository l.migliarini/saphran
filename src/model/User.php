<?php

require_once '../src/model/Model.php';

class User extends Model {

    protected $User_Id;
    protected $Mail;
    protected $Nom;
    protected $Role_Id;
    protected $Password;

    public function construct(array $data=null) {
        parent::construct();
        if ($data != null) {
            $this->User_Id=$data["idUser"];
            $this->Mail=$data["toLogin"];
            $this->Nom=$data["Nom"];
            $this->Role_Id=$data["idRole"];
            $this->Password=$data["password"];
        }
    }

    function getUser_Id() {
        return $this->User_Id;
    }

    function getMail() {
        return $this->Mail;
    }

    function getNom() {
        return $this->Nom;
    }

    function getRole_Id() {
        return $this->Role_Id;
    }

    function getPassword() {
        return $this->Password;
    }

    function setUser_Id($User_Id): void {
        $this->User_Id = $User_Id;
    }

    function setMail($Mail): void {
        $this->Mail = $Mail;
    }

    function setNom($Nom): void {
        $this->Nom = $Nom;
    }

    function setRole_Id($Role_Id): void {
        $this->Role_Id = $Role_Id;
    }

    function setPassword($Password): void {
        $this->Password = $Password;
    }
}
