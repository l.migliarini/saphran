<?php

require_once 'Model.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor..
 */

/**
 * Description of CountryLanguages
 *
 * @author student
 */
class CountryLanguage extends Model{
    
    protected $CountryLanguage_Id;
    protected $CountryCode ;
    protected $Language;
    protected $IsOfficial ;
    protected $Percentage; 
    
    public function __construct(array $data=NULL) {
        parent::__construct();
        if($data = null){
            $this->CountryLanguage_Id=$data["CountryLanguage_Id"];
            $this->CountryCode=$data["CountryCode"];
            $this->Language=$data["Language"];
            $this->IsOfficial=$data["IsOfficial"];
            $this->Percentage=$data["Percentage"];
            
        }
        
    }
            
    function getCountryLanguage_Id() {
        return $this->CountryLanguage_Id;
    }

    function getCountryCode() {
        return $this->CountryCode;
    }

    function getLanguage() {
        return $this->Language;
    }

    function getIsOfficial() {
        return $this->IsOfficial;
    }

    function getPercentage() {
        return $this->Percentage;
    }

    function setCountryLanguage_Id($CountryLanguage_Id): void {
        $this->CountryLanguage_Id = $CountryLanguage_Id;
    }

    function setCountryCode($CountryCode): void {
        $this->CountryCode = $CountryCode;
    }

    function setLanguage($Language): void {
        $this->Language = $Language;
    }

    function setIsOfficial($IsOfficial): void {
        $this->IsOfficial = $IsOfficial;
    }

    function setPercentage($Percentage): void {
        $this->Percentage = $Percentage;
    }

    
            
    
   
}
