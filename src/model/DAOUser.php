<?php

require_once '../src/model/Model.php';
require_once '../src/model/User.php';
require_once '../src/model/DAO.php';

class DAOUser extends DAO {

    public $cnx;

    public function __construct(PDO $cnx) {
        parent::__construct($cnx);
    }

    public function connexion($Mail, $Password) {
        $sql = "SELECT * FROM User WHERE toLogin = :Mail AND password = :Password";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Mail", $Mail);
        $preparedStatement->bindValue("Password", $Password);
        $preparedStatement->execute();
        $user = $preparedStatement->fetchObject("user");
        if ($user == "") {
            return null;
        }
        return $user;
    }

    public function find($Id): ?User {
        $sql = "SELECT * FROM User WHERE idUser = :User_Id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("User_Id", $Id);
        $preparedStatement->execute();
        $users = $preparedStatement->fetchObject("User");
        if ($users == "") {
            return NULL;
        }
        return $users;
    }

    function count(): int {
        $sql = "SELECT COUNT (idUser) FROM User";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->execute();
        $count_user = $preparedStatement->fetchColumn();
        return $count_user;
    }

    public function getUserPermissions($roleId) {
        $sql = "SELECT * FROM ROLES WHERE idRole = :roleId";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("roleId", $roleId);
        $preparedStatement->execute();
        $role = $preparedStatement->fetch();
        if ($role == "") {
            return null;
        }
        return $role['Permission'];
    }

    function save($user) {
        $sql = "INSERT INTO User (toLogin, password, Nom, idRole) VALUES (:Mail, :Password, :Nom, :idRole)";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("Mail", $user->getMail());
        $preparedStatement->bindValue("Password", $user->getPassword());
        $preparedStatement->bindValue("Nom", $user->getNom());
        $preparedStatement->bindValue("idRole", $user->getRole_Id());
        $preparedStatement->execute();
    }

    function update($user) {
        $sql = "UPDATE User SET toLogin = :Mail, password = :Password, Nom = :Nom WHERE idUser = :User_Id";
        $preparedStatement = $this->cnx->prepare($sql);
        $preparedStatement->bindValue("User_Id", $user->getUser_Id());
        $preparedStatement->bindValue("Mail", $user->getMail());
        $preparedStatement->bindValue("Nom", $user->getNom());
        $preparedStatement->bindValue("Password", $user->getPassword());
        $preparedStatement->execute();
    }

    function remove($id) {
        $sqlUser = "DELETE FROM User WHERE idUser = :id";
        $preparedStatementUser = $this->cnx->prepare($sqlUser);
        $preparedStatementUser > bindValue("id", $id);
        $preparedStatementUser->execute();
    }

    public function findAll(): array {
        $SQL = "SELECT * FROM User";
        $preparedStatement = $this->cnx->query($SQL);
        $preparedStatement->setFetchMode(PDO::FETCH_CLASS, 'User');
        $preparedStatement->execute();
        $user_list = [];

        while (($data = $preparedStatement->fetchObject("User")) != false) {
            array_push($user_list, $data);
        }
        return $user_list;
    }

}
