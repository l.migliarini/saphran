<?php
require_once '../src/model/Model.php';
require_once '../src/model/City.php';
require_once '../src/model/DAO.php';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DAOCity extends DAO {
    
    public function __construct( $cnx ) 
    {
        parent::__construct($cnx);
    }
    
    public function find($id) : City 
    {
        $SQL = "SELECT * FROM city WHERE City_id = :id";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("id", $id);
        $prepareStatement -> execute();
        $City = $prepareStatement -> fetchObject("City");
        return $City;
    }

    public function count(): int
    {
        $SQL = "SELECT COUNT(City_id) FROM city";
        $prepareStatement = $this->cnx->query($SQL);
        $prepareStatement->execute();
        $country_count = $prepareStatement->fetchColumn();
        return $country_count;
    }

    public function findAll(): array {
        $SQL = "SELECT * FROM city";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> execute();
        $liste = [];
        while(($data = $prepareStatement ->fetchObject("City")) != false){
            array_push($liste,$data);
        }
        return $liste;
    }

    public function remove($id) {
        $SQL = "DELETE from city where City_id = :id";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("id", $id);
        $prepareStatement -> execute();
    }

    public function save($City){
        
       $SQL = "INSERT INTO city (Name, CountryCode, District, Population) values (:name, :countrycode, :district, :population)";
       $prepareStatement = $this-> cnx-> prepare($SQL);
       $prepareStatement -> bindValue ("name", $City->getName());
       $prepareStatement -> bindValue ("countrycode", $City->getCountryCode());
       $prepareStatement -> bindValue ("district", $City->getDistrict());
       $prepareStatement -> bindValue ("population", $City->getPopulation());
       $prepareStatement -> execute();
    }

     public function update($city){
        
       $sql = "UPDATE city SET ";
        foreach ($city->getObjectVar() as $key => $value) {
            if ($value != NULL) {
                $sql .= " $key = :$key,";
            }
        }
        $sql = rtrim($sql, ",");
        $sql .= " WHERE City_Id = :City_Id";
        $prepareStatement = $this->cnx->prepare($sql);
        foreach ($city->getObjectVar() as $key => $value) {
            if ($value != NULL) {
                $method = "get$key";
                if (method_exists($city, $method)) {
                    $prepareStatement->bindValue($key, $city->$method());
                }
            }
        }
        $prepareStatement->execute();
    }
    
        public function getAllCityFromCountry($id) {
        $SQL = "SELECT * FROM city WHERE CountryCode =:id";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("id", $id);
        $prepareStatement -> execute();
        $city = $prepareStatement -> fetchAll(PDO::FETCH_ASSOC);
        return $city;  
    }
    
    public function findCountryByCity($id){
        $SQL = "SELECT * FROM country WHERE Code =:id";
        $prepareStatement = $this-> cnx-> prepare($SQL);
        $prepareStatement -> bindValue ("id", $id);
        $prepareStatement -> execute();
        $countrybycity = $prepareStatement -> fetchAll(PDO::FETCH_ASSOC);
        return $countrybycity;
        
    }


}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   /*public function FindCityById($params = [])
    {
        $SQL = "SELECT * FROM city WHERE City_Id=:id";

        $city = $this->find($SQL,$params, 'city');

        return $city;
    }


    public function UpdateById($params = [])
    {
        $SQL = "INSERT INTO city (Name, CountryCode, District, Population) values (:name, :countrycode, :district, :population)";

        $city = $this->save($SQL,$params, 'city');

        return $city;
    }
    
    public  function SaveByCity ($city)
    {
        $SQL = "INSERT INTO city (Name, CountryCode, District, Population) values (:name, :countrycode, :district, :population)";
        
    }
    
    public function RemoveByCity($params = []){
        $SQL = "DELET from city where City_id = :id";
        
        $city = $this->remove($SQL,$params, 'city');

        return $city;
    }*/
    

