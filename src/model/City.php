<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class City extends Model{
    
    protected $City_Id;
    protected $Name;
    protected $CountryCode;
    protected $District;
    protected $Population;


    public function __construct( array $data=NULL ) 
    {
        parent::__construct();
        if($data != null){
            $this->City_Id=$data["City_Id"];
            $this->Name=$data["Name"];
            $this->CountryCode=$data["CountryCode"];
            $this->District=$data["District"];
            $this->Population=$data["Population"];
        }
    }
    
    function getCity_Id() {
        return $this->City_Id;
    }

    function getName() {
        return $this->Name;
    }

    function getCountryCode() {
        return $this->CountryCode;
    }

    function setCity_Id($City_Id): void {
        $this->City_Id = $City_Id;
    }

    function setName($Name): void {
        $this->Name = $Name;
    }

    function setCountryCode($CountryCode): void {
        $this->CountryCode = $CountryCode;
    }

    function getDistrict() {
        return $this->District;
    }

    function getPopulation() {
        return $this->Population;
    }

    function setDistrict($District): void {
        $this->District = $District;
    }

    function setPopulation($Population): void {
        $this->Population = $Population;
    }


}