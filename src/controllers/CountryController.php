<?php

require_once 'BaseController.php';
require_once '../src/utils/Utils.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/model/DAOUser.php';

/**
 * Description of CountryController
 *
 * @author student
 */
class CountryController {

    /** @var DAOCountry $daoCountry */
    private $daoCountry;

    use Csrftoken;

    public function __construct() {
        $this->daoCountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
        $this->daoUser = new DAOUser(SingletonDataBase::getInstance()->cnx);
    }


    public function show($id) {
        //verifier validiter id
        $country = $this->daoCountry->find($id);
        $page = Renderer::Render("liste_pays.php", compact("country"));
        echo $page;
    }

    public function showAllCountryByContinent($id) {
        $country = $this->daoCountry->getAllCityFromContinent($id);
        $permissions = $this->daoUser->getUserPermissions("country");
        $page = Renderer::Render("liste_pays.php", compact("country"));
        echo $page;
    }

    public function showEdit($id) {
        $country = $this->daoCountry->find($id);
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("EditCountry.php", compact("country", "csrf_token"));
        echo $page;
    }

    /** MAJ d'un pays* */
    public function DoEdit($id) {

        if (isset($_POST['Country_Id'])) {
            $Country_Id = htmlspecialchars($_POST['Country_Id']);
        }

        if (isset($_POST['Name'])) {
            $Name = htmlspecialchars($_POST['Name']);
        }

        if (isset($_POST['Continent'])) {
            $Continent = htmlspecialchars($_POST['Continent']);
        }

        if (isset($_POST['Region'])) {
            $Region = htmlspecialchars($_POST['Region']);
        }

        if (isset($_POST['SurfaceArea'])) {
            $SurfaceArea = htmlspecialchars($_POST['SurfaceArea']);
        }

        if (isset($_POST['IndepYear'])) {
            $IndepYear = htmlspecialchars($_POST['IndepYear']);
        }

        if (isset($_POST['Population'])) {
            $Population = htmlspecialchars($_POST['Population']);
        }

        if (isset($_POST['LifeExpectancy'])) {
            $LifeExpectancy = htmlspecialchars($_POST['LifeExpectancy']);
        }

        if (isset($_POST['GNP'])) {
            $GNP = htmlspecialchars($_POST['GNP']);
        }

        if (isset($_POST['GNPOld'])) {
            $GNPOld = htmlspecialchars($_POST['GNPOld']);
        }

        if (isset($_POST['LocalName'])) {
            $LocalName = htmlspecialchars($_POST['LocalName']);
        }

        if (isset($_POST['GovernmentForm'])) {
            $GovernmentForm = htmlspecialchars($_POST['GovernmentForm']);
        }

        if (isset($_POST['HeadOfState'])) {
            $HeadOfState = htmlspecialchars($_POST['HeadOfState']);
        }

        if (isset($_POST['Capital'])) {
            $Capital = htmlspecialchars($_POST['Capital']);
        }

        if (isset($_POST['Image1'])) {
            $Image1 = htmlspecialchars($_POST['Image1']);
        }

        if (isset($_POST['Image2'])) {
            $Image2 = htmlspecialchars($_POST['Image2']);
        }

        $data = [ "Name" => $Name, "Capital" => $Capital, "Population" => $Population];
        $filter = new Filter($data);
        $filter ->acceptVisitor("Name", $this.NameVisitor::class);
        $filter ->acceptVisitor("Capital", $this.CapitalVisitor::class);
        $filter ->acceptVisitor("Population", $this.PopulationVisitor::class);
        
        $country = $this->daoCountry->find($id);

        $country->setCountry_Id($Country_Id);
        $country->setName($Name);
        $country->setContinent($Continent);
        $country->setRegion($Region);
        $country->setSurfaceArea($SurfaceArea);
        $country->setIndepYear($IndepYear);
        $country->setPopulation($Population);
        $country->setLifeExpectancy($LifeExpectancy);
        $country->setGNP($GNP);
        $country->setGNPOld($GNPOld);
        $country->setLocalName($LocalName);
        $country->setGovernmentForm($GovernmentForm);
        $country->setHeadOfState($HeadOfState);
        $country->setCapital($Capital);
        $country->setImage1($Image1);
        $country->setImage2($Image2);

        if (isset($_POST['csrf_token'])) {
            $csrf_token = htmlspecialchars($_POST['csrf_token']);

            if ($this->check($csrf_token) == false) {
                $csrf_token = $this->generateToken();
                $page = Renderer::Render("EditCountry.php", compact("country", "csrf_token"));
                echo $page;
                return null;
            }
        }

        $this->daoCountry->update($country);

        header('Location: http://localhost:8888/country/showCity/' . $country->getCode());
    }

    public function showAdd() {
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("AddCountry.php", compact("csrf_token"));
        echo $page;
    }

    public function DoAdd() {

        if (isset($_POST['Name'])) {
            $Name = htmlspecialchars($_POST['Name']);
        }

        if (isset($_POST['CountryCode'])) {
            $Code = htmlspecialchars($_POST['CountryCode']);
        }

        if (isset($_POST['Continent'])) {
            $Continent = htmlspecialchars($_POST['Continent']);
        }

        if (isset($_POST['Region'])) {
            $Region = htmlspecialchars($_POST['Region']);
        }

        if (isset($_POST['SurfaceArea'])) {
            $SurfaceArea = htmlspecialchars($_POST['SurfaceArea']);
        }

        if (isset($_POST['IndepYear'])) {
            $IndepYear = htmlspecialchars($_POST['IndepYear']);
        }

        if (isset($_POST['Population'])) {
            $Population = htmlspecialchars($_POST['Population']);
        }

        if (isset($_POST['LifeExpectancy'])) {
            $LifeExpectancy = htmlspecialchars($_POST['LifeExpectancy']);
        }

        if (isset($_POST['GNP'])) {
            $GNP = htmlspecialchars($_POST['GNP']);
        }

        if (isset($_POST['GNPOld'])) {
            $GNPOld = htmlspecialchars($_POST['GNPOld']);
        }

        if (isset($_POST['LocalName'])) {
            $LocalName = htmlspecialchars($_POST['LocalName']);
        }

        if (isset($_POST['GovernmentForm'])) {
            $GovernmentForm = htmlspecialchars($_POST['GovernmentForm']);
        }

        if (isset($_POST['HeadOfState'])) {
            $HeadOfState = htmlspecialchars($_POST['HeadOfState']);
        }

        if (isset($_POST['Capital'])) {
            $Capital = htmlspecialchars($_POST['Capital']);
        }

        if (isset($_POST['Code2'])) {
            $Code2 = htmlspecialchars($_POST['Code2']);
        }

        if (isset($_POST['Image1'])) {
            $Image1 = htmlspecialchars($_POST['Image1']);
        }

        if (isset($_POST['Image2'])) {
            $Image2 = htmlspecialchars($_POST['Image2']);
        }

        $country = new Country();

        $country->setCode($Code);
        $country->setName($Name);
        $country->setContinent($Continent);
        $country->setRegion($Region);
        $country->setSurfaceArea($SurfaceArea);
        $country->setIndepYear($IndepYear);
        $country->setPopulation($Population);
        $country->setLifeExpectancy($LifeExpectancy);
        $country->setGNP($GNP);
        $country->setGNPOld($GNPOld);
        $country->setLocalName($LocalName);
        $country->setGovernmentForm($GovernmentForm);
        $country->setHeadOfState($HeadOfState);
        $country->setCapital($Capital);
        $country->setCode2($Code2);
        $country->setImage1($Image1);
        $country->setImage2($Image2);

//        
//        if (isset($_POST['csrf_token'])) {
//            $csrf_token = htmlspecialchars($_POST['csrf_token']);
//
//            if ($this->check($csrf_token) == false) {
//                $csrf_token = $this->generateToken();
//                $page = Renderer::Render("AddCountry.php", compact("csrf_token"));
//                echo $page;
//                return null;
//            }
//        }

        $this->daoCountry->save($country);

        header('Location: http://localhost:8888/country/showCountry/' . $country->getContinent());
    }

    public function showDelete($id) {
        $country = $this->daoCountry->find($id);
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("DeleteCountry.php", compact("country", "csrf_token"));
        echo $page;
    }

    public function DoDelete($id) {
       
        $country = $this->daoCountry->remove($id);
         header('Location: http://localhost:8888/');
    }

}
