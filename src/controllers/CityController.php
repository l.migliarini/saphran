<?php
require_once 'BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCity.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/Paginator.php';
require_once '../src/model/DAOUser.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CityController
 *
 * @author student
 */
class CityController extends BaseController{
    /** @var DAOCity $daocity*/
    private $daocity;
    
    use Csrftoken;
    
    use Paginator;
    
    private $limit = 10;
    
    public function __construct() {
        $this->daocity = new DAOCity(SingletonDataBase::getInstance()->cnx);
        $this->daoUser = new DAOUser(SingletonDataBase::getInstance()->cnx);
    }
    
    
    /*Affichage des villes*/
    public function display($id = null)
    {
        if ($id != null)
        {
            $city = $this->daocity->find($id);
            $view = Renderer::render("pays.php", compact('city'));
            echo $view; 
        }
        else {
            $city = $this->daocity->findAll();
            $view = Renderer::render("pays.php", compact('city'));
            echo $view; 
        }
    }
    
    /** Afficher details des pays **/
    
     public function showAllCityByCountry($id) {
        $city = $this->daocity->getAllCityFromCountry($id);
        $countrybycity = $this->daocity->findCountryByCity($id);
        $permissions = $this->daoUser->getUserPermissions("city");
        
        $url = rtrim("?page=", $_SERVER['REQUEST_URI']);
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $count = $this->daocity->count($id);
        $paginator = $this->paginate($url, $page, $count, $this->limit);

        $page = Renderer::Render("details_pays.php", compact("city","countrybycity","paginator", "page","permissions"));
        echo $page;  
    }
    
    /** Afficher la page editer ville **/
    
    public function showEdit($id) {
        $city = $this->daocity->find($id);
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("editCity.php", compact("city","csrf_token"));
        echo $page; 
    }
    
      /** MAJ d'une ville **/
    
    public function DoEdit($id) {
        
        if ($this->check("csrf_token")==false)
        {
            //TODO utiliser un renderer et afficher une page error
        }
        
        if (isset($_POST['City_id']))
        {
            $city_id = htmlspecialchars($_POST['City_id']);
        }
        if (isset($_POST['Name']))
        {
            $name = htmlspecialchars($_POST['Name']);
        }
        if (isset($_POST['District']))
        {
            $district = htmlspecialchars($_POST['District']);
        }
        if (isset($_POST['Population']))
        {
            $population = htmlspecialchars($_POST['Population']);
        }
        if (isset($_POST['CountryCode']))
        {
            $countryCode = htmlspecialchars($_POST['CountryCode']);
        }

        
        
        $city = $this->daocity->find($id);
        
        $city->setCity_Id($city_id);
        $city->setName($name);
        $city->setDistrict($district);
        $city->setPopulation($population);
        $city->setCountryCode($countryCode);
        
        $this->daocity->update($city);
        
        header('Location: http://localhost:8888/country/showCity/' . $city->getCountryCode() . '?page=1');

    }

    public function Dodelete($id)
    {
        $cities = $this->daocity->find($id);
        $city = $this->daocity->remove($id);
        
        header('Location: http://localhost:8888/country/showCity/' . $cities->getCountryCode() . '?page=1');
    }
    
    public function add()
    {
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("AddCity.php", compact("csrf_token"));
        echo $page;
    }
    
    public function DoAdd() {

        if (isset($_POST['Name'])) {
            $Name = htmlspecialchars($_POST['Name']);
        }

        if (isset($_POST['CountryCode'])) {
            $CountryCode = htmlspecialchars($_POST['CountryCode']);
        }

        if (isset($_POST['District'])) {
            $District = htmlspecialchars($_POST['District']);
        }


        if (isset($_POST['Population'])) {
            $Population = htmlspecialchars($_POST['Population']);
        }


        $city = new City();

        $city->setCountryCode($CountryCode);
        $city->setName($Name);
        $city->setDistrict($District);
        $city->setPopulation($Population);
        


        $this->daocity->save($city);

        header('Location: http://localhost:8888/country/showCity/' . $city->getCountryCode() . '?page=1');
    }
}
