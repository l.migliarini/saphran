<?php

require_once 'BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCountryLanguage.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/model/DAOUser.php';

class CountryLanguageController extends BaseController {
    /** @var DAOCountryLanguage $daocountrylanguage*/
    private $daocountrylanguage;

    public function __construct() {
        $this->daocountrylanguage= new DAOCountryLanguage(SingletonDataBase::getInstance()->cnx);
        $this->daoCountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
        $this->daoUser = new DAOUser(SingletonDataBase::getInstance()->cnx);
    }

    public function showCreate() {
        $view = Renderer::Render("edit_langage.php");
        echo $view;
    }

    public function show($id) {
        $language = $this->daocountrylanguage->find($id);
        $view = Renderer::Render("langage.php", compact("language"));
        echo $view;
    }

    public function showLanguagesForCountry($countryCode) {
        $country = $this->daoCountry->findByCode($countryCode);
        $languages = $this->daocountrylanguage->findByCoutry($countryCode);
        $permissions = $this->daoUser->getUserPermissions("lang");
        //$csrf_token = $this->generateToken();
        $view = Renderer::render("liste_langages_pays.php", compact("country", "languages","permissions"));
        echo $view;
    }

    public function showEdit($languageId) {
        $language = $this->daocountrylanguage->find($languageId);
        $token = getToken();
        $rootUrl = getConfVal('rootUrl');
        $publichtmlRootUrl = getConfVal('publichtmlRootUrl');
        $view = Renderer::Render("edit_language.php", compact('token', 'rootUrl', 'publichtmlRootUrl', 'language'));
        echo $view; 
    }

    public function update($language){
        $countryCode = $language->getCountryCode();
        $this->daocountrylanguage->update($language);
        $countryCode = $language->getCountryCode();
        $this->showLanguagesForCountry($countryCode);
    }

    public function remove($languageId){
        $language = $this->daocountrylanguage->find($languageId);
        $countryCode = $language->getCountryCode();
        $this->daocountrylanguage->remove($language);
        $this->showLanguagesForCountry($countryCode);
    }
}

