<?php

require_once 'BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOUser.php';
require_once '../src/utils/CsrfToken.php';
require_once '../src/utils/auth.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author student
 */
class UserController {

    private $daoUser;

    use Csrftoken;

    public function __construct() {
        $this->daoUser = new DAOUser(SingletonDataBase::getInstance()->cnx);
    }

    public function deconnexion_user() {
        auth::logout();
        header('Location: http://localhost:8888/');
    }

    public function showUser() {
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("Connexion.php", compact("csrf_token"));
        echo $page;
    }

    public function showDeco() {
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("Deconnexion.php", compact("csrf_token"));
        echo $page;
    }

    public function DoConnexion() {

        if (isset($_POST['Mail'])) {
            $Mail = htmlspecialchars($_POST['Mail']);
        }

        if (isset($_POST['Password'])) {
            $Password = htmlspecialchars($_POST['Password']);
        }


        $User = $this->daoUser->connexion($Mail, $Password);

        if ($User !== "" || null) {
            
            auth::login($User);
            header('Location: http://localhost:8888/');
        }

//        if (isset($_POST['csrf_token'])) {
//            $csrf_token = htmlspecialchars($_POST['csrf_token']);
//
//            if ($this->check($csrf_token) == false) {
//                $csrf_token = $this->generateToken();
//                return null;
//            }
//        }
        else {
            header('Location: http://localhost:8888/user/showConnexion/');
        }
        

        // Get user role.
        $userPermissions = $this->daoUser->getUserPermissions($User->getRole_Id());

        // Generate token.
        $token = generateToken($User, $userPermissions);

    }
    
    public function Showadd()
    {
        $csrf_token = $this->generateToken();
        $page = Renderer::Render("Inscription.php", compact("csrf_token"));
        echo $page;
    }
    
    public function DoAdd() {

        if (isset($_POST['Email'])) {
            $email = htmlspecialchars($_POST['Email']);
        }

        if (isset($_POST['Password'])) {
            $password = htmlspecialchars($_POST['Password']);
        }

        if (isset($_POST['Nom'])) {
            $nom = htmlspecialchars($_POST['Nom']);
        }


        $user = new User();

        $user->setMail($email);
        $user->setPassword($password);
        $user->setNom($nom);
        $user->setRole_Id(3);
        


        $this->daoUser->save($user);

         header('Location: http://localhost:8888');
    }

}
