<?php

require_once '../src/controllers/BaseController.php';
require_once '../src/utils/Renderer.php';
require_once '../src/controllers/CountryController.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/SingletonDataBase.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultController
 *
 * @author student
 */
class DefaultController {
    
    /** @var DAOCountry $daoCountry */
    private $daoCountry;
    
    public function __construct() {
        $this->daoCountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
    }
    
    
    public function show() {
        
        $enum = $this->daoCountry->getEnumFromContinent();
        $continent = [];
        foreach ($enum as $value){
            $continent[$value['Continent']]= $value['Continent'];
        }
        $page = Renderer::render("welcome.php", compact("continent"));
        echo $page;
        
    }
}
