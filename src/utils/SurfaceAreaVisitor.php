<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SurfaceAreaVisitor
 *
 * @author student
 */
class SurfaceAreaVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $surface = (string) $data;
        if (strlen ($surface)<=12 && preg_match('@[0-9]@', $surface)){
            return true;
        }
        else{
            return false;
        }
    }
}
