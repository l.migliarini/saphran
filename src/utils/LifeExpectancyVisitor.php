<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LifeExpectancyVisitor
 *
 * @author student
 */
class LifeExpectancyVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $lifeExpect = (string) $data;
        if (strlen ($lifeExpect)<=4 && preg_match('@[0-9]@', $lifeExpect)){
            return true;
        }
        else{
            return false;
        }
    }
}
