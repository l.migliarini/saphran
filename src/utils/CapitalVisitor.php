<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CapitalVisitor
 *
 * @author student
 */
class CapitalVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $Capital = (string) $data;
        if (strlen ($Capital)<=11 && preg_match('@[0-9]@', $Capital)){
            return true;
        }
        else{
            return false;
        }
    }
}
