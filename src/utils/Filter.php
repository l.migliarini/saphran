<?php

require_once '../src/utils/AbstactVisitor.php';
require_once '../src/utils/CapitalVisitor.php';
require_once '../src/utils/CodeVisitor.php';
require_once '../src/utils/Code2Visitor.php';
require_once '../src/utils/ContinentVisitor.php';
require_once '../src/utils/GNPOldVisitor.php';
require_once '../src/utils/GNPVisitor.php';
require_once '../src/utils/GovernementFormVisitor.php';
require_once '../src/utils/HeadOfStateVisitor.php';
require_once '../src/utils/ImageVisitor.php';
require_once '../src/utils/Image2Visitor.php';
require_once '../src/utils/IndepYearVisitor.php';
require_once '../src/utils/LifeExpectancyVisitor.php';
require_once '../src/utils/LocalNameVisitor.php';
require_once '../src/utils/NameVisitor.php';
require_once '../src/utils/PopulationVisitor.php';
require_once '../src/utils/RegionVisitor.php';
require_once '../src/utils/SurfaceAreaVisitor.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Filter
 *
 * @author student
 */
class Filter {
    
    public $visitors = [];


    public function __construct($formData) {
        foreach ($formData as $key => $value) {
            $this->$key = $value;
        }
    }
    
    public function acceptVisitor(string $key, AbstractVisitor $visitor) {
        $visit = 'Visit'.ucfirst($key);
        if (method_exists($visitor, $visit))
        $this->visitors[$key] = $visitor->visite($this->$key);
    }
    
    public function visite() { //retourne boolean dans un premier temps
        foreach ($this->visitors as $key=>$visitor){
            if($visitor ==false){
                return false;
            }
            else {
                return true;
            }
        }
    }
}

