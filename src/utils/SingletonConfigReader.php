<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingletonConfigReader
 *
 * @author student
 */
class SingletonConfigReader {
    
    private $config;
    
    private static $instance=null; 

    private function __construct()
    {
        $this->config = parse_ini_file(filter_input(INPUT_SERVER,'DOCUMENT_ROOT').'/../conf.ini', true);
    }
    
    public static function getInstance() : SingletonConfigReader
    {
        if(self::$instance == null)
        {
            self::$instance = new SingletonConfigReader();
        }
        return self::$instance;
    }
    
    function getValue(string $key, string $section = null): ?string{
	if($section != null)
        {
            return $this->config[$section][$key];
        }
        else
        {
            return $this->config[$key];
        }
        
    }
}
