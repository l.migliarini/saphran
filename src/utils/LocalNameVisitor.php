<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LocalNameVisitor
 *
 * @author student
 */
class LocalNameVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $LocalName = (string) $data;
        if (strlen ($LocalName)<=45 && preg_match('@[A-Z]@', $LocalName) && preg_match('@[a-z]@', $LocalName)){
            return true;
        }
        else{
            return false;
        }
    }
}
