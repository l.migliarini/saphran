<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PopulationVisitor
 *
 * @author student
 */
class PopulationVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $population = (string) $data;
        if (strlen ($population)<=11 && preg_match('@[0-9]@', $population)){
            return true;
        }
        else{
            return false;
        }
    }
}
