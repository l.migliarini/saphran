<?php

trait Csrftoken {
    
    private static $token_name = "csrf_token";
    
    public function generateToken() : string {
        
        $this->start();
        $token  =   md5(uniqid(microtime(), true));
        $_SESSION[self::$token_name] = $token; 

        return $_SESSION[self::$token_name];
        
    }

    public function check(string $key) : bool {
        
        $this->start();
        // I would clear the token after matched
        if($key == $_SESSION[self::$token_name]) {
            $_SESSION[self::$token_name]   =   NULL;
            return true;
        }
        // I would return false by default, not true
        return false;
    }
    
    public function start(){
        if (session_id() ==""){
            session_start();
        }
    }
}
