<?php

// =================================================================================================
/** Stop executing the current script. */
function stop() {
    exit();
}
// =================================================================================================
// =================================================================================================
function stringify($obj) {
    return print_r($obj, true);
}
// =================================================================================================
// =================================================================================================
function echoel() {
    echo '<br/>';
}
// -------------------------------------------------------------------------------------------------
function echol($text) {
    if ($text != null) {
        echo $text . '<br/>';
    } else {
        echo '<br/>';
    }
}
// =================================================================================================
// =================================================================================================
function echopre($text) {
    if ($text != null) {
        echo "<pre>" . $text . "</pre>";
    } else {
        echo "<pre></pre>";
    }
}
// =================================================================================================
// =================================================================================================
function echoHtml($html) {
    echopre(htmlspecialchars($html));
}
// =================================================================================================
// =================================================================================================
function echosep() {
    echo "=================================================<br/>";
}
// =================================================================================================
// =================================================================================================
function popup($text) {
    echo "<script>alert('" . $text . "')</script>";
}
// =================================================================================================
// =================================================================================================
function pause(){
    popup('');
}
// =================================================================================================
// =================================================================================================
function getRequestInfo(){
    $r = new stdClass();

    $rootUrl = getConfVal('rootUrl');

    $r->httpMethod = strtoupper($_SERVER["REQUEST_METHOD"]);
    $r->queryParams = $_REQUEST;

    $r->path = '';
    $r->explodedPath = [];
    if (isset($_SERVER["REQUEST_URI"])) {
        $uri = $_SERVER["REQUEST_URI"];
        $xUri = explode('?', $uri);
        $r->path = $xUri[0];
        $r->explodedPath = explode('/', $r->path);
        if ((count($r->explodedPath) > 0) && ($r->explodedPath[0] == '')) {
            array_shift($r->explodedPath);
        }
        if ((count($r->explodedPath) > 0) && ($r->explodedPath[0] == $rootUrl)) {
            array_shift($r->explodedPath);
            $r->path = implode('/', $r->explodedPath);
        }
    } else {
        return false;
    }

    // Convert object to associative array.
    $r = get_object_vars($r);

    return $r;
}
// =================================================================================================
// =================================================================================================
function requestMatches($httpMethodAndPath, $requestInfo){
    $httpMethodAndPathA = explode(' ', $httpMethodAndPath);

    list(
        $httpMethod,
        $path
    ) = $httpMethodAndPathA;

    return (
        ($httpMethod == $requestInfo['httpMethod'])
        &&
        (gettype(strpos($requestInfo['path'], $path)) == 'integer')
    );
}
// =================================================================================================
// =================================================================================================
function getConfVal($confValIdSpec){
    $section =  '';
    $key = $confValIdSpec;

    $xConfValIdSpec = explode('.', $confValIdSpec);

    if (count($xConfValIdSpec) == 2) {
        $section = array_shift($xConfValIdSpec);
        $key = array_shift($xConfValIdSpec);
    }

    if ($section != '') {
        return SingletonConfigReader::getInstance()->getValue($key, $section);
    } else {
        return SingletonConfigReader::getInstance()->getValue($key);
    }
}
// =================================================================================================
// =================================================================================================
function array_append_element(&$array, $times){
    for ($i = 1; $i <= $times; $i++) {
        array_push($array, $array[0]);
    }
}
// =================================================================================================
// =================================================================================================
function generateToken($User, $userPermissions) {
    $token = base64_encode($User->getMail() . ' ' . $userPermissions);
    return str_replace('=', '', $token);
}
// =================================================================================================
// =================================================================================================
function getToken() {
    $queryParams = $GLOBALS['queryParams'];

    if (isset($queryParams['token'])) {
        return $queryParams['token'];
    }

    return '';
}
// =================================================================================================
// =================================================================================================
function decodeToken($token) {
    return base64_decode($token . str_repeat('=', strlen($token) % 4));
}
// =================================================================================================
// =================================================================================================
function getCurrentPermissions($entityKey) {
    $token = getToken();

    if ($token == '') {
        return '';
    }

    $token = decodeToken($token);
    $explodedToken = explode(' ', $token);
    $explodedToken = explode(',', $explodedToken[1]);
    foreach ($explodedToken as $entity) {
        $explodedEntity = explode(':', $entity);
        $key = $explodedEntity[0];
        $val = $explodedEntity[1];
        if ($key == $entityKey) {
            return $val;
        }
    }

    return base64_decode($token . str_repeat('=', strlen($token) % 4));
}
// =================================================================================================
?>
