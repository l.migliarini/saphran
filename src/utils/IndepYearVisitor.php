<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndepYearVisitor
 *
 * @author student
 */
class IndepYearVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $indep = (string) $data;
        if (strlen ($indep)<=6 && preg_match('@[0-9]@', $indep)){
            return true;
        }
        else{
            return false;
        }
    }
}
