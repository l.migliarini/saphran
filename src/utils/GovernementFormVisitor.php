<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GovernementStatementVisitor
 *
 * @author student
 */
class GovernementFormVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $GovForm = (string) $data;
        if (strlen ($GovForm)<=45 && preg_match('@[A-Z]@', $GovForm) && preg_match('@[a-z]@', $GovForm)){
            return true;
        }
        else{
            return false;
        }
    }
}
