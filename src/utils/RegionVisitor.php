<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegionVisitor
 *
 * @author student
 */
class RegionVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $region = (string) $data;
        if (strlen ($region)<=26 && preg_match('@[A-Z]@', $region) && preg_match('@[a-z]@', $region)){
            return true;
        }
        else{
            return false;
        }
    }
}
