<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GNPOldVisitor
 *
 * @author student
 */
class GNPOldVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $GNPold = (string) $data;
        if (strlen ($GNPold)<=12 && preg_match('@[0-9]@', $GNPold)){
            return true;
        }
        else{
            return false;
        }
    }
}
