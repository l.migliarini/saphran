<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Code2Visitor
 *
 * @author student
 */
class Code2Visitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $Code2 = (string) $data;
        if (strlen ($Code2)==2 && preg_match('@[A-Z]@', $Code2)){
            return true;
        }
        else{
            return false;
        }
    }
}
