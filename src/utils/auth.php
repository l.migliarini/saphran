<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class auth {

    static $KEY = "USERSESSION";
    static $CANDELETE = "D";
    static $CANCREATE = "C";
    static $CANUPDATE = "U";

    
  /*  public static function login(User $user) {
        session_start();
        $_SESSION['Nom'] = $user->getNom();
        $_SESSION['idRole'] = $user->getRole_Id();
        var_dump($_SESSION['Name']);
        header('Location: /');
        echo "Vous êtes connecté";
    }
   * */
   
    public static function login(User $user) {
        self::checkSession();
        $_SESSION[self::$KEY] = $user;
    }

    public static function logout() {
        self::checkSession();
        if (isset($_SESSION[self::$KEY])) {
            session_destroy();
        }
    }

    public static function isLoged(): bool {
        self::checkSession();
        if (isset($_SESSION[self::$KEY])) {
            return true;
        }

        return false;
    }
    
    public static function hasRole(string $role): bool {
        self::checkSession();
        $userPerm = $_SESSION[self::$KEY]->getUser_roles();
        $rolesUser = [];
        if($userPerm - self::$CANCREATE >= 0){
            $userPerm -= self::$CANCREATE;
            $rolesUser[] = "C";
        }
        if($userPerm - self::$CANREAD >= 0){
            $userPerm -= self::$CANREAD;
            $rolesUser[] = "R";
        }
        if($userPerm - self::$CANUPDATE >= 0){
            $userPerm -= self::$CANUPDATE;
            $rolesUser[] = "U";
        }
        if($userPerm - self::$CANDELETE >= 0){
            $userPerm -= self::$CANDELETE;
            $rolesUser[] = "D";
        }
        if(in_array($role, $rolesUser)){
            return true;
        }
        return false;
    }

    /**
     * Check if current user has required permission
     * @param int $perm
     * @return bool
     */
    public static function can(int $perm): bool {
        self::checkSession();
        $role = "";
        switch ($perm) {
            case self::$CANCREATE :
                $role = "C";
                break;
            case self::$CANREAD :
                $role = "R";
                break;
            case self::$CANUPDATE :
                $role = "U";
                break;
            case self::$CANDELETE  :
                $role = "D";
                break;
            default:
                break;
        }
        if(self::hasRole($role)){
            return true;
        }
        return false;
    }


    public static function checkSession() {
        if (session_id() == "") {
            session_start();
        }
    }

}
