<?php
require_once '../src/utils/SingletonConfigReader.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingletonDataBase
 *
 * @author student
 */
class SingletonDataBase {
    
    /** @VAR PDO $cnx*/
    public $cnx;
    
    private static $serveur;
    private static $database;
    
    private static $username;
    private static $password;
    
    /** @var SingletonDataBase  $instance */
    private static $instance=null; 

    private function __construct()
    {
        /*self::$serveur="127.0.0.1"; //on utilise self et non this car c'est static
        self::$database="worlddb";
        self::$username="worlddbuser";
        self::$password="123+aze";*/

        self::$serveur= SingletonConfigReader::getInstance()->getValue("serveur");
        self::$database= SingletonConfigReader::getInstance()->getValue("database","worlddb");
        self::$username= SingletonConfigReader::getInstance()->getValue("username","worlddb");
        self::$password= SingletonConfigReader::getInstance()->getValue("password","worlddb");
        
        $this->cnx = new PDO("mysql:host=" . self::$serveur. ";dbname=". self::$database, self::$username, self::$password);

    }
    
    public static function getInstance() : SingletonDataBase
    {
        if(self::$instance == null)
        {
            self::$instance = new SingletonDataBase();
        }
        return self::$instance;
    }
}
