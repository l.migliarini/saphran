<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CodeVisitor
 *
 * @author student
 */
class CodeVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $code = (string) $data;
        if (strlen ($code)==3 && preg_match('@[A-Z]@', $code)){
            return true;
        }
        else{
            return false;
        }
    }
}
