<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NameVisitor
 *
 * @author student
 */
class NameVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $name = (string) $data;
        if (strlen ($name)<=52 && preg_match('@[A-Z]@', $name) && preg_match('@[a-z]@', $name)){
            return true;
        }
        else{
            return false;
        }
    }
}
