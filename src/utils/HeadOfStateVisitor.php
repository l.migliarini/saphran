<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HeadOfStateVisitor
 *
 * @author student
 */
class HeadOfStateVisitor extends AbstractVisitor{
    public function visite(string $data) : bool {
        $HeadOfState = (string) $data;
        if (strlen ($HeadOfState)<=60 && preg_match('@[A-Z]@', $HeadOfState) && preg_match('@[a-z]@', $HeadOfState)){
            return true;
        }
        else{
            return false;
        }
    }
}
