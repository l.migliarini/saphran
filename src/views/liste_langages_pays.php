<!DOCTYPE html>
<html>
    <head>
        <title>World Data</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img class="card-img-top" src="img/monde.png" alt="Card image cap"></a>
            <a class="navbar-brand" href="#">World Data</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../user/showConnexion">Connexion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../user/showDeconnexion">Deconnexion</a>
                    </li>
                    <li class="nav-item">
                        <p class="nav-link" href=#>  <?php if (auth::isLoged()): ?><?php echo "user: " . $_SESSION[auth::$KEY]->getNom(); ?> <?php endif; ?></p>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>

        <br><br>

        <h1 style="text-align : center;" class="mb-5">Langages pour le pays <?php echo $country->getName(); ?></h1>
        <div class="text-center mb-5">
            <img class="drapeau"src="<?php
            if ($country->getImage1() != null) {
                echo $country->getImage1();
            } else {
                echo $country->getImage2();
            }
            ?>"/> 
        </div>
        <div class="row justify-content-md-center">
            <div class="col-10">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Id</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Langue officielle</th>
                            <th scope="col">Pourcentage</th>
                            <th scope="col">Editer</th>
                        </tr>
                    </thead> 
<?php $i = 0;
foreach ($languages as $countrylanguage): $i++
    ?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $countrylanguage['CountryLanguage_Id']; ?></td>
                            <td><?php echo $countrylanguage['Language'] ?></td>
                            <td><?php echo $countrylanguage['IsOfficial']; ?></td>
                            <td><?php echo $countrylanguage['Percentage']; ?></td>
                            <td>
     <?php if (auth::isLoged()): ?><?php if ($_SESSION[auth::$KEY]->idRole) {
                            if ($_SESSION[auth::$KEY]->idRole == 1||2) {?>
                                    <a href="/language/edit/<?php echo $countrylanguage['CountryLanguage_Id']; ?>"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pen" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                                        </svg>
                                    </a>
     <?php } ?>
                                    <?php } ?>
                                    <?php endif; ?>
     <?php if (auth::isLoged()): ?><?php if ($_SESSION[auth::$KEY]->idRole) {
                            if ($_SESSION[auth::$KEY]->idRole == 1) {?>
                                    <a href="/language/remove/<?php echo $countrylanguage['CountryLanguage_Id']; ?>"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pen" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
                                    </a>
                         <?php } ?>
                                    <?php } ?>
                                    <?php endif; ?>
                            </td>
                        </tr>
<?php endforeach; ?>
                </table>
            </div>
        </div>
        <nav aria-label="...">
            <ul class="pagination justify-content-center mt-5">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item"><a class="page-link" href="#">6</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>


    </div>
</div>
</body>
</html>
