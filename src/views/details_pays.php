<!DOCTYPE html>
<html>
    <head>
        <title>World Data</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/pays.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img class="card-img-top" src="img/monde.png" alt="Card image cap"></a>
            <a class="navbar-brand" href="#">World Data</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../user/showConnexion">Connexion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../user/showDeconnexion">Deconnexion</a>
                    </li>
                    <li class="nav-item">
                        <p class="nav-link" href=#>  <?php if (auth::isLoged()): ?><?php echo "user: " . $_SESSION[auth::$KEY]->getNom(); ?> <?php endif; ?></p>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>

        <div class="titre">Liste des villes de <?php echo $countrybycity[0]["Name"]; ?></div>

        <div class="text-center mb-5">
            <img class="drapeau" src="<?php
            if ($countrybycity[0]["Image1"] == null) {
                echo $countrybycity[0]["Image1"];
            } else {
                echo $countrybycity[0]["Image2"];
            }
            ?>">
        </div>

        <div class="row justify-content-md-center">
            <div class="col-10">

                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <th scope="col">id</th>
                            <th><?php echo $countrybycity[0]["Country_Id"]; ?></th>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <th scope="row">nom</th>
                            <td><?php echo $countrybycity[0]["Name"]; ?></td>

                        </tr>
                        <tr>
                            <th scope="row">Continent</th>
                            <td><?php echo $countrybycity[0]["Continent"]; ?></td>

                        </tr>
                        <tr>
                            <th scope="row">Capital</th>
                            <td colspan="2"><?php echo $countrybycity[0]["Capital"]; ?></td>

                        </tr>
                        <tr>
                            <th scope="row">Population</th>
                            <td colspan="2"><?php echo $countrybycity[0]["Population"]; ?></td>
                        </tr>
                    </tbody>
                </table>


                <div class="row justify-content-md-center">
                    <div class="col-10">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">id</th>
                                    <th scope="col">Nom</th>
                                    <th scope="col">District</th>
                                    <th scope="col">Population</th>
                                    <th scope="col">
                                        <?php if (auth::isLoged()): ?><?php
                                            if ($_SESSION[auth::$KEY]->idRole) {
                                                if ($_SESSION[auth::$KEY]->idRole == 1) {
                                                    ?>
                                                    <a href="/city/showAdd/">
                                                        <svg width="1em" 
                                                             height="1em" 
                                                             viewBox="0 0 16 16" 
                                                             class="bi bi-plus-square-fill" 
                                                             fill="currentColor" 
                                                             xmlns="http://www.w3.org/2000/svg%22%3E">
                                                        <path fill-rule="evenodd" 
                                                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                                        </svg>
                                                    </a>
                                                <?php } ?>
                                            <?php } ?>
<?php endif; ?>

                                    </th>
                                </tr>
                            </thead>
                            <body>
                            <tbody>
                                <?php
                                $scope = $page * 10 - 10;
                                for ($indent = $page * 10 - 10; $indent < ceil($page * 10); $indent++): $scope++; /* var_dump($value); */
                                    ?>
    <?php if (isset($city[$indent])): ?>
                                        <tr>
                                            <th scope="row"><?= $scope; ?></th>
                                            <td><?= $city[$indent]['City_Id']; ?></td>

                                            <td><?= $city[$indent]["Name"]; ?></td>
                                            <td><?= $city[$indent]['District']; ?></td>
                                            <td><?= $city[$indent]['Population']; ?></td>

                                            <td><?php if (auth::isLoged()): ?><?php
                                                    if ($_SESSION[auth::$KEY]->idRole) {
                                                        if ($_SESSION[auth::$KEY]->idRole == 1||2) {
                                                            ?>
                                                            <a href="/city/edit/<?= $city[$indent]["City_Id"]; ?>"><svg width="1em" 
                                                                                                                        height="1em" 
                                                                                                                        viewBox="0 0 16 16" 
                                                                                                                        class="bi bi-pen-fill" 
                                                                                                                        fill="currentColor" 
                                                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                                                        style="color:green">
                                                                <path fill-rule="evenodd" 
                                                                      d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>
                                                                </svg></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php endif; ?>
                                                <?php if (auth::isLoged()): ?><?php
                                        if ($_SESSION[auth::$KEY]->idRole) {
                                            if ($_SESSION[auth::$KEY]->idRole == 1) {
                                                ?>
                                                            <a href="/city/delete/<?= $city[$indent]["City_Id"]; ?>"><svg width="1em" 
                                                                                                                          height="1em" 
                                                                                                                          viewBox="0 0 16 16" 
                                                                                                                          class="bi bi-trash-fill" 
                                                                                                                          fill="currentColor" 
                                                                                                                          xmlns="http://www.w3.org/2000/svg"
                                                                                                                          style="color:red">
                                                                <path fill-rule="evenodd" 
                                                                      d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                                                </svg>
                                                            </a>
                <?php } ?>
                                            <?php } ?>
                                        <?php endif; ?>
                                            </td>
                                        </tr>
    <?php endif; ?>
<?php endfor; ?>
                            </tbody>
                        </table>
                        <br>
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-3">
                                <nav aria-label="...">
<?php echo $paginator; ?>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
                </body>




                </html>