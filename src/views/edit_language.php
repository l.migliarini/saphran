<!DOCTYPE html>
<html>
    <head>
        <title>World Data</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#"><img class="card-img-top" src="img/monde.png" alt="Card image cap"></a>
            <a class="navbar-brand" href="#">World Data</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../user/showConnexion">Connexion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../../user/showDeconnexion">Deconnexion</a>
                    </li>
                    <li class="nav-item">
                        <p class="nav-link" href=#>  <?php if (auth::isLoged()): ?><?php echo "user: " . $_SESSION[auth::$KEY]->getNom(); ?> <?php endif; ?></p>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
<body>
    <form action="/language/update/<?php echo $language->getCountryLanguage_Id(); ?>?token=<?php echo $token; ?>" method="POST">
        <div class="form-group">
            <div class="col-sm-3 my-1">
                <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">id:</div>
                    </div>
                    <input readonly type="text" class="form-control" name="CountryLanguage_id" value="<?php echo $language->getCountryLanguage_Id()?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3 my-1">
                    <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Code pays</div>
                        </div>
                        <input type="text" class="form-control" name="CoutryCode" value="<?php echo $language->getCountryCode()?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-3 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Langue</div>
                            </div>
                            <input type="text" class="form-control" name="Language" value="<?php echo $language->getLanguage()?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3 my-1">
                            <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Langue officielle </div>
                                </div>
                                <input type="checkbox" value="T" class="form-control" name="IsOfficial" <?php if ($language->getIsOfficial() == 'T') {echo " checked";} ?> >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3 my-1">
                                <label class="sr-only" for="inlineFormInputGroupUsername"></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Pourcentage</div>
                                    </div>
                                    <input type="text" class="form-control" name="Percentage" value="<?php echo $language->getPercentage()?>">
                                </div>
                            </div>
                            <!-- <input type="hidden" name="csrf_token" value="<?php echo $csrf_token; ?>">  -->
                            <button type="submit" class="btn btn-primary" data-toggle="collapse">Valider</button>
                            <button type="reset" class="btn btn-primary" data-toggle="collapse">Reset</button>
                            <button type="button" class="btn btn-primary" onclick="history.go(-1)">Back</button>
    </form>
</body>
</html>
