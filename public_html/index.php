<?php

require_once '../src/controllers/CityController.php';
require_once '../src/controllers/CountryController.php';
require_once '../src/controllers/DefaultController.php';
require_once '../src/controllers/UserController.php';
require_once '../src/controllers/CountryLanguageController.php';

//echo "<pre>" . print_r($_SERVER, true) . "<pre>";

if (isset($_SERVER["PATH_INFO"])) {
    $path = trim($_SERVER["PATH_INFO"], "/");
} else {
    $path = "";
}

$fragments = explode("/", $path);

//var_dump($fragment);

$control = array_shift($fragments);
switch ($control) {
    case "city" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            } elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
                cityRoutes_post($fragments);
            }
            break;
        }

    case "country" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                countryRoutes_get($fragments);
            } elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
                countryRoutes_post($fragments);
            }
            break;
        }

    case "user": {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                userRoutes_get($fragments);
            } elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
                userRoutes_post($fragments);
            }
            break;
        }
    case "language" : {
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            } elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
                cityRoutes_post($fragments);
            }
            break;
        }

    default : {
            $defaultcontroller = new DefaultController();
            $defaultcontroller->show();
        }
}

function cityRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "detail" : {
                call_user_func_array([new CityController, "display"], $fragments);
                break;
        }
        
        case "showAdd": {
                call_user_func_array([new CityController(), "add"], $fragments);
                break;
            }
        
        case "edit" : {
                call_user_func_array([new CityController, "showEdit"], $fragments);
                break;
        }
        
        case "delete" : {
                call_user_func_array([new CityController, "Dodelete"], $fragments);
                break;
        }
    }
}

function cityRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "edit" : {
                call_user_func_array([new CityController, "DoEdit"], $fragments);
                break;
            }
            
        case "delete" : {
                call_user_func_array([new CityController, "Dodelete"], $fragments);
                break;
        }
        
        case "Add": {
                call_user_func_array([new CityController(), "DoAdd"], $fragments);
                break;
            }
    }
}

function countryRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "showEdit" : {
                call_user_func_array([new CountryController, "showEdit"], $fragments);
                break;
            }
        case "showCountry" : {
                call_user_func_array([new CountryController(), "showAllCountryByContinent"], $fragments);
                break;
            }
        case "showCity": {
                call_user_func_array([new CityController(), "showAllCityByCountry"], $fragments);
                break;
            }
        case "showAdd": {
                call_user_func_array([new CountryController(), "showAdd"], $fragments);
                break;
            }
        case "showDelete": {
                call_user_func_array([new CountryController, "showDelete"], $fragments);
                break;
            }
        case "showLanguagesForCountry": {
                call_user_func_array([new CountryLanguageController(), "showLanguagesForCountry"], $fragments);
                break;
            }
    }
}

function countryRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "edit" : {
                call_user_func_array([new CountryController(), "DoEdit"], $fragments);
                break;
            }
        case "Add": {
                call_user_func_array([new CountryController(), "DoAdd"], $fragments);
                break;
            }
        case "Delete": {
                call_user_func_array([new CountryController(), "DoDelete"], $fragments);
                break;
            }
    }
}

function userRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "showConnexion": {
                call_user_func_array([new UserController(), "showUser"], $fragments);
                break;
            }
        case "showDeconnexion": {
                call_user_func_array([new UserController(), "showDeco"], $fragments);
                break;
            }
        case "showInscription": {
                call_user_func_array([new UserController(), "Showadd"], $fragments);
                break;
            }
    }
}

function userRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "connexion": {
                call_user_func_array([new UserController(), "DoConnexion"], $fragments);
                break;
            }
        case "deconnexion": {
                call_user_func_array([new UserController(), "deconnexion_user"], $fragments);
                break;
            }
        case "inscription": {
                call_user_func_array([new UserController(), "DoAdd"], $fragments);
                break;
            }
    }
}

function languageRoutes_get($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "edit" : {
                call_user_func_array([new CountryLanguageController(), "showEdit"], $fragments);
                break;
            }
        case "remove" : {
                call_user_func_array([CountryLanguageController(), "remove"], $fragments);
                break;
            }
    }
}

function languageRoutes_post($fragments) {

    $action = array_shift($fragments);

    switch ($action) {

        case "update": {
                call_user_func_array([new CountryLanguageController(), "update"], $fragments);
                break;
            }
    }
}

?>